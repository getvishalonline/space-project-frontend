import Head from 'next/head'
import styles from '../styles/Home.module.css'
import React, { useEffect, createRef } from 'react';


export default function Home() {

  let piInput = createRef();
  let radiusInput = createRef();
  let result = createRef();

  const calculatePi = function() {
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/sun_circumference`, {
      method: 'POST',
      cors: true,
      headers: {
        'Content-Type': 'application/json',
      }
    })
    .then(response => response.json())
    .then(function(data) {
      console.log(data)
      piInput.current.value = data.pi;
      result.current.innerHTML = data.circumference;
    })
  }

  useEffect(() => {
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/get_info`)
    .then(response => response.json())
    .then(function(data) {
      console.log(data)
      piInput.current.value = data.pi;
      result.current.innerHTML = data.circumference;
      radiusInput.current.value = data.radius;
    })
  }, []);

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        

        <p className={styles.description}>
        </p>

        <div className={styles.container}>
        <h2 className={styles.title}>
          Welcome to Project Solar!
        </h2>
          <div className={styles.inputContainer}>
            <label>Pi (π)</label>
            <textarea placeholder="Pi" ref={piInput} readOnly="readonly" ></textarea>
            <label>Radius of Sun</label>
            <input type="text" placeholder="Radius" readOnly="readonly" ref={radiusInput}/>
            <button type="button" onClick={calculatePi}>Recalculate</button>
          </div>
          <div className={styles.outputContainer}>
            <h4>Circumference Of Sun</h4>
            <p ref={result}></p>
          </div>
        </div>
      </main>

      <footer className={styles.footer}>
      </footer>
    </div>
  )
}
